package ca.cegepdrummond;
import java.util.Scanner;

public class Serie4_EntreesClavier {
    /*
     * Modifiez ce code afin qu'il affiche le nombre qui sera fourni en entré.
     * Important: affichez seulement l'entier, rien d'autre.
     *
     * Note: seul des entiers seront fournis par les tests.
     */
    public void clavier1() {
        Scanner clavier = new Scanner(System.in);
        System.out.print("Entrez un numéro  :" );
        int num1 = clavier.nextInt();


        System.out.println(num1);   // Ajouter la commande nécessaire pour afficher l'entier qui a été entré au clavier.

    }

    /*
     * Corrigez ce code afin qu'il affiche le nombre qui sera fourni en entré.
     */
    public void clavier2() {

        Scanner clavier = new Scanner(System.in);
        System.out.print("Entrez un numéro  :" );
        int num1 = clavier.nextInt();
        System.out.println(num1);

    }

    /*
     * Modifiez ce code afin qu'il demande une chaine de caractère suivit d'un entier
     * (ils peuvent être sur la même ligne ou sur deux lignes différentes).
     * Vous devez ensuite afficher la chaine de caractère sur la ligne et l'entier sur la ligne suivante.
     */
    public void clavier3() {


        Scanner clavier = new Scanner(System.in);
        System.out.print("Entrez une phrase :");
        String chaine1 = clavier.nextLine();
        System.out.print("Entrez un nombre  :");
        int num1 = clavier.nextInt();
        System.out.println(chaine1);
        System.out.println(num1 );





    }

    /*
     * Programmer une fonction qui demande 3 chaines de caractères et les affiche ensuite dans l'ordre inverse.
     * Exemple:
     * monde
     * le
     * allo
     *
     * affichera:
     * allo
     * le
     * monde
     *
     */
    public void clavier4() {

        Scanner clavier = new Scanner(System.in);
        System.out.print("monde :");
        String chaine1 = clavier.nextLine();
        System.out.println(chaine1);
        System.out.print("le :");
        String chaine2 = clavier.nextLine();
        System.out.println(chaine2);
        System.out.print("allo :");
        String chaine3 = clavier.nextLine();
        System.out.println(chaine3);
        System.out.println("allo le monde" );
        System.out.println(chaine3);
        System.out.println(chaine2);
        System.out.println(chaine1);



    }

    /*
     * Modifiez ce code pour qu'il demande 5 mots sur une même ligne et les affiches sur des lignes successives.
     *
     * Exemple:
     * Les cinq mots à lire
     *
     * affichera:
     * Les
     * cinq
     * mots
     * à
     * lire
     *
     * indice: next vs nextLine.
     */
    public void clavier5() {
        Scanner clavier = new Scanner(System.in);

        String mot1 = clavier.next();
        String mot2 = clavier.next();
        String mot3 = clavier.next();
        String mot4 = clavier.next();
        String mot5 = clavier.next();
        System.out.println(mot1);
        System.out.println(mot2);
        System.out.println(mot3);
        System.out.println(mot4);
        System.out.println(mot5);

    }

    /*
     * Modifiez ce code afin qu'il inverse les deux valeurs entrées.
     *
     * exemple:
     * un
     * deux
     *
     * affichera
     * deux un
     *
     *
     * Note: cette technique pour inverser 2 valeurs en utilisant une valeur intermédiaire
     * est souvent utilisée.
     * Si vous ne comprenez pas le fonctionnement, veuillez demander au professeur.
     *
     */
    public void clavier6() {
        Scanner s = new Scanner(System.in);
        String valeur1 = s.next();
        String valeur2 = s.next();
        String intermediaire;
        intermediaire = valeur1;
        valeur1 = valeur2;
        valeur2 = intermediaire;
        System.out.println(valeur1 + " " + valeur2); // <<<< ne modifiez pas cette ligne

    }



}