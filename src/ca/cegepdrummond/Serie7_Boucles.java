package ca.cegepdrummond;

import java.util.Scanner;

public class Serie7_Boucles {
    /*
     * Modifiez le code afin que la boucle affiche les nombres de 0 à 5 (inclusivement) sur des lignes différentes
     *  suivit de "fin"
     *
     * utilisez une boucle while.
     *
     * Exemple de résultat:
     * 0
     * 1
     * 2
     * 3
     * 4
     * 5
     * fin
     */
    public void while1() {
        int nombre = 0;


        while(nombre <= 5) {
            System.out.println(nombre);
            nombre = nombre + 1;
        }


        System.out.println("fin");

    }

    /*
     * Modifiez le code afin d'afficher les entiers entre les 2 nombres en entrés suivit de "fin"
     * .
     * Pour simplifier le problème, le premier nombre entré doit être plus petit que le deuxième.
     *
     * utilisez une boucle while.
     *
     * Exemple:
     * 2
     * 7
     * affichera:
     * 2
     * 3
     * 4
     * 5
     * 6
     * 7
     * fin
     *
     * * Attention: les 2 chiffres peuvent aussi être égaux. Dans ce cas, comptez de x à x
     * Exemple:
     * 5
     * 5
     * affichera:
     * 5
     * fin
     */

    public void while2() {

        Scanner s = new Scanner(System.in);
        int n1 = s.nextInt();
        int n2 = s.nextInt();

        int compteur = n1;
        while(n1 <= n2) {
            System.out.println(compteur);
          compteur = compteur + 1;
          n1++;
          /* compteur+=1;
          *  compteur++;
          *  */
        }

        System.out.println("fin");

    }

    /*
     * Modifiez le code afin d'afficher les entiers entre les 2 nombres en entrés, suivit de "fin".
     * Cette fois-ci, le premier entier peut être plus petit ou plus grand que le deuxième.
     * S'il est plus petit, vous devez compter du deuxième au premier.
     *
     * utilisez une boucle while.
     *
     * Par exemple, si les deux entiers sont 8 et 6, le résultat doit être
     * 6
     * 7
     * 8
     *
     * Vous devez donc trouver quelle entrée est la plus petite et l'utiliser
     * comme valeur de départ (compteur), et prendre l'autre comme valeur de fin (plusGrand).
     *
     * Attention, ici aussi les deux nombres peuvent être égaux.
     */
    public void while3() {
        Scanner s = new Scanner(System.in);
        int n1 = s.nextInt();
        int n2 = s.nextInt();
        int compteur = n1;
        while (compteur<= n2) {
            System.out.println(compteur);
            compteur= compteur + 1;
        }

        System.out.println("fin");

    }

    /*
     * Modifiez le code afin de trouver la somme d'une suite de nombre qui sera fournie en entrée.
     * La fin de la suite est indiquée par la valeur 0.
     *
     * utilisez une boucle do while.
     *
     * exemple:
     * 3 4 7 -1 0
     * affichera:
     * 13
     *
     *
     */
    public void dowhile1() {
        Scanner s = new Scanner(System.in);
        int total = 0;
        int nombre;

        do {
            nombre = s.nextInt();
            total = total + nombre;

        } while (s.nextInt()!=0);
         System.out.println(total);
         }






    /*
     * Modifiez le code afin de trouver le plus grand entier fourni en entrée.
     * La suite de nombre en entrée se termine par un 0.
     *
     * utilisez une boucle do while
     *
     * Note: dans le code, Integer.MIN_VALUE est une constante représentant
     * la valeur minimum qu'un entier peut prendre.
     *
     * Note: si vous entrez 0 dès le départ, alors 0 sera le résultat.
     */
    public void dowhile2() {
        Scanner s = new Scanner(System.in);
        int plusGrand = Integer.MIN_VALUE;
        int nombre;

        do {
            nombre = s.nextInt();
           if (nombre>plusGrand) {
               plusGrand=nombre;
           }

        } while (s.nextInt()!=0);
        System.out.println(plusGrand);
    }


    /*
     * Modifiez le code afin de trouver le nombre d'entier en entrée avant la valeur 0.
     * Utilisez une boucle do while
     * S'il y a des entiers entrés après le 0, ils ne doivent pas être comptés.
     * Si seul la valeur 0 est entrée, le résultat est 0.
     *
     * Exemple:
     * 3 2 5 1 42 0
     * affichera:
     * 5
     *
     * Exemple 2:
     * 0
     * affichera 0
     *
     * Exemple 3:
     * 1 1 1 1 0 2
     * affichera:
     * 4
     *
     */
    public void dowhile3() {
        Scanner s = new Scanner(System.in);
        int compteur = 0;
        int nombre;

        do {
            nombre = s.nextInt();
            if (nombre>compteur) {
                compteur=compteur+nombre;
            }

        } while (nombre!=0);
        System.out.println(compteur);


    }
    
    /*
     * Modifiez le code pour que la boucle affiche sur une ligne les entiers se trouvant entre les deux valeurs entrées.
     *
     * utilisez une boucle for
     *
     * Exemple: si les valeurs en entrées sont 4 et 9, la boucle doit afficher "4 5 6 7 8 9 " (sans les "").
     *
     * Note: les nombres doivent être affichés sur la même ligne, séparés par un espace, la liste se termine par un espace.
     *
     * Note: Pour simplifier le problème, la première valeur doit être plus petite que la deuxième.
     *
     * Note: les deux nombres en entrées peuvent être égaux.
     *
     */
    public void for1() {
        Scanner s = new Scanner(System.in);
        int nombre1 = s.nextInt();
        int nombre2 = s.nextInt();


        for (int i= nombre1; i< 10; i++) {
            System.out.println(i);




                System.out.println(""); //conserver cette ligne

    }
    }

    /*
     * Modifiez le code pour que la boucle affiche tous les nombres pairs entre 0 et 20 inclusivement.
     *
     * indice: l'incrément de la boucle for n'est pas nécessairement 1.
     *
     * resultat:
     * "0 2 4 6 8 10 12 14 16 18 20 " (sans les guillemets)
     */
    public void for2() {

        for (int i = 0; i <= 20; i+=2){

            System.out.println(i);






        System.out.println(""); // conserver cette ligne.

    }
    }
    /*
     * Modifiez le code pour que la boucle affiche tous les multiples de 3 entre 0 et 20 dans l'ordre descendant
     *
     * résultat:
     * "18 15 12 9 6 3 0 " (sans les guillemets)

     * indice: modulo pour trouver les multiples de 3
     * indice: l'incrément de la bouche peut être négatif.
     */
    public void for3() {

        for (int i = 20; i>=0; i--){
            if (i%3==0)
                System.out.println(i);

        }
            System.out.println(""); // conserver cette ligne.

    }

    /*
     * Modifiez le code afin d'afficher à l'envers le mot fourni en entrée.
     * Choisissez la boucle que vous désirez ( for, while, do while)
     *
     * exemple:
     * allo
     * affichera:
     * olla
     *
     * exemple:
     * kayak
     * affichera:
     * kayak
     *
     * indice: longueur d'une chaine de caractères pour la boucle
     * indice: décomposition d'une chaine de caractères. Nous avons vu une fonction permettant d'aller
     *         chercher un caractère à une certaine position dans la chaine de caractères.
     * indice: le dernier caractère est à la position chaine.length()-1.
     *
     */

    public void boucle1() {
        Scanner s = new Scanner(System.in);
        String chaine = s.next();
        String initial = "allo ";
        StringBuffer sb = (new StringBuffer(initial)).reverse();
        System.out.println("sb = " + sb);
         System.out.println(""); //conserver cette ligne.
        }















    /*
     * Modifiez le code afin de compter combien il y a de lettre et de chiffre dans la phrase fournie en entrée.
     * Vous devez choisir le type de boucle à utiliser.
     * Vous devez afficher:
     * lettres: x
     * nombres: y
     *
     * en remplaçant x et y par le nombre de lettres et de nombres.
     *
     * Note: un espace n'est ni une lettre ni un nombre.
     * Note: il se peut qu'il n'y est pas de lettres ou pas de nombres dans l'entré.
     *       Dans ce cas, vous devez afficher 0 pour le comptes de lettres ou les nombres.
     *
     * indice: il y a des fonctions pour évaluer un char.
     */
        public void boucle2 () {
            int compteurLettres = 0, compteurNombres = 0;
            Scanner s = new Scanner(System.in);
            String chaine = s.nextLine();
            for(int i = 0; i<chaine.length(); i++){
                char lettre = chaine.charAt(i);
                if ((int) lettre >= 97 && (int) lettre <=  122)
                    compteurLettres++;
                else if ((int) lettre >= 49 && (int) lettre <= 57)
                    compteurNombres++;

            }
            System.out.println("lettres: "+compteurLettres);
            System.out.println("nombres: "+compteurNombres);


        }

    /*
     * Vous devez faire une boucle qui continuera tant que l'usager n'entrera pas le nombre 0.
     *
     * Vous devez aussi vérifier que l'entrée est composée d'un seul caractère et que celui-ci est un "digit".
     *
     * Si l'usager entre plus d'un caractère avant de faire "entré", vous devez afficher le message
     *   "entrez un seul caractère"
     *
     * Si l'usager entre un seul caractère, mais que celui-ci n'est pas un nombre, vous devez afficher le message
     *   "entrez un nombre de 0 à 9"
     *
     * Si l'usager n'entre aucun caractère et fait "entrée", affichez
     *   "entrez au moins 1 caractère"
     *
     * Quand l'usager entre un nombre entre 0 et 9, affichez "bravo" et terminez la boucle.
     *
     * Exemple:
     * allo
     * affichera: entrez un seul caractère
     *
     * 22
     * affichera: entrez un nombre de 0 à 9
     *
     * 4
     * affichera: bravo
     * et terminera la boucle.
     */
    public void boucle3() {
        Scanner s = new Scanner(System.in);
        boolean fin = false;

        while (!fin){
            String entree = s.nextLine();
            if (entree.length() > 1) {
                System.out.println("entrez un seul caractère");
            } else if (entree.length() == 0) {
            } else if (Character.isLetter(entree.charAt(0))) {
                System.out.println("entrez un nombre de 0 a 9");

                System.out.println("entrez au moins 1 caractère");
            } else {
                fin = true;
                System.out.println("bravo");
            }

        }
    }

    /*
     * Modifiez le code afin qu'il transforme en majuscule le premier caractère de chaque mot de la phrase entrée.
     * Vous devez le faire en vérifiant les caractères un par un (n'utilisez pas de fonctions de String que nous n'avons pas vue)
     * Un espace précède toujours un mot, et bien entendu la phrase commence par un mot.
     *
     * Les tests n'utiliseront que des minuscules et ne contiendront que des lettres et des espaces (pas de chiffres)
     */

    public void boucle4() {
        Scanner s = new Scanner(System.in);
        boolean nouveauMot = true;    // on sait que le premier caractère débute un nouveau mot.
        String chaine = s.nextLine();

        int position = 0;

        do {
            if(nouveauMot) {
                System.out.print(Character.toUpperCase(chaine.charAt(position)));  // afficher le caractère en majuscule
                nouveauMot=false; // que faut-il faire pour indiquer de PAS mettre le prochain caractère en majuscule ?

            } else if(chaine.charAt(position) == ' ' ) {  // détection d'un espace, donc il faudra mettre le prochain caractère en majuscule.
                nouveauMot=true; ; // que faut-il faire pour indiquer de mettre le prochain caractère en majuscule ?
                System.out.print(" ");
            } else  { // traitement d'un caractère normal.
                System.out.print(chaine.charAt(position));
            }
            position++;


        System.out.println(""); //conserver cette ligne
    } while (position < chaine.length());

    }

    /*
     * Modifiez le code afin de détecter les palindromes (mot s'écrivant de la même facon à l'envers (ex: kayak)
     *
     * Vous devez donc vérifier si la première lettre est la même que la dernière, la deuxième égale à l'avant dernière
     * et ainsi de suite.
     *
     * Certains palindromes ont un nombre pair de caractères (ex: serres), d'autres un nombre impair (kayak). Vous devez
     * traiter les 2 cas.
     *
     * indice: si c'est impair, vous n'avez pas à vérifier la lettre du centre.
     *
     * Si c'est palindrome, affichez "oui", si non affichez "non".
     */
    public void boucle5() {
        Scanner s = new Scanner(System.in);
        String chaine = s.nextLine();
        boolean palindrome = true; // on présume que ce sera un palindrome
        int i2 = chaine.length();
        for(int i=0; i < chaine.length() ; i++) {
            i2--;
            if ( chaine.charAt(i) != chaine.charAt(i2) ) { // vérifier si le caractère à la position i est différent du ième caractère de la fin
                palindrome = false; // que doit-on faire pour indiquer que ce n'est pas un palindrome?
            }
        }
        if(palindrome) {
            System.out.println("oui");
        } else {
            System.out.println("non");
        }

        }


}
